CWD := $(abspath $(patsubst %/,%,$(dir $(abspath $(lastword $(MAKEFILE_LIST))))))
LIBDIR ?= /opt/extra/lib/libxft


all: dmenu/dmenu emoji.txt

dmenu/dmenu: dmenu libxft/src/.libs/libXft.a
	make -C $<

dmenu:
	git clone --branch main https://gitlab.com/xzva/dmenu
	patch -d $@ < dmenu.patch
	@# Add existing files to the Git index to make it easier to
	@# build a user customization patch of the config.
	git -C $@ add -u

libxft/src/.libs/libXft.a: libxft
	cd $(LIBDIR) && ./autogen.sh && make SUBDIRS=src

libxft: libxft-bgra.patch
	sudo rm -rf /opt/extra/lib/libxft
	sudo git clone https://gitlab.freedesktop.org/xorg/lib/libxft /opt/extra/lib/libxft
	@# Remove check for xorg-util-macros that's only used to add `.1` at the
	@# end of a man page we're not gonna use.
	sudo patch -d $(LIBDIR) < libxft.patch
	sudo patch -d $(LIBDIR) -p1 < $<

libxft-bgra.patch:
	curl -o $@ https://gitlab.freedesktop.org/xorg/lib/libxft/-/merge_requests/1.patch

emoji.txt:
	curl https://raw.githubusercontent.com/fdw/rofimoji/main/src/picker/data/emojis.csv \
		| sed -E 's,<\/?small>,,g' > $@

install: all
	sudo mkdir -p $(DESTDIR)$(PREFIX)/bin
	sudo cp -f $(CWD)/dmenu/dmenu $(CWD)/dmenu/dmenu_* $(CWD)/dmenu/stest -t $(DESTDIR)$(PREFIX)/bin
	sudo chmod 755 $(DESTDIR)$(PREFIX)/bin/dmenu*
	sudo chmod 755 $(DESTDIR)$(PREFIX)/bin/stest
	sudo mkdir -p $(DESTDIR)$(MANPREFIX)/man1
	sed -i "s/VERSION/$(VERSION)/g" $(CWD)/dmenu/dmenu.1
	sed -i "s/VERSION/$(VERSION)/g" $(CWD)/dmenu/stest.1 
	sudo cp -f $(CWD)/dmenu/dmenu.1 $(CWD)/dmenu/stest.1 -t $(DESTDIR)$(MANPREFIX)/man1
	sudo chmod 644 $(DESTDIR)$(MANPREFIX)/man1/dmenu.1
	sudo chmod 644 $(DESTDIR)$(MANPREFIX)/man1/stest.1

uninstall: 
	rm -rf $(LIBDIR) \
	            $(DESTDIR)$(PREFIX)/bin/dmenu \
		    $(DESTDIR)$(PREFIX)/bin/dmenu_path \
		    $(DESTDIR)$(PREFIX)/bin/dmenu_run \
		    $(DESTDIR)$(PREFIX)/bin/stest \
		    $(DESTDIR)$(MANPREFIX)/man1/dmenu.1 \
		    $(DESTDIR)$(MANPREFIX)/man1/stest.1


